 Commandrepo
 General commands used daily 
Ssh (username@ip address -I path) to access server
Sudo kill -9 process id to kill a running process
Sudo apt – get update this is used before installing new package to make sure manager is up to date
Sudo apt-get install package to install a package 
Sudo apt-get install upgrade to upgrade you installed packages 
Sudo df -h to check human readable format
Sudo du -h -d 1 /var/ to check which part of your system is consuming lots of disk space
Sudo htop is used to see how a process are consuming memory, cpu and which cores are under pressure
F5 to show all process in a tree view to debug the subprocess
Sudo ps aux to view all process id, where a=show processes for all users; u=display the process’s user/owner; x=also show processes not attached to a terminal
Tail -f /var/log/somelog.log to check what is going on in your log file without scanning through the whole file during a debugging process
Cat /var/log/somelog.log to output a file or concatenate some files for outputting to the console
Ps -ef shows process status
Env allows you to set or print the environment variables especially during troubleshooting to view which what is wrong with your application
Top to determine how much cpu and memory your application consumes 
Netstat shows the network status
Isof list the open files associated with your application
Df -h to troubleshoot disk space issues (display free disk space)
Du -sh /var/log/* to retrieve more detailed information about which files use the disk space in a directory
MIGRATION FROM LAUNCHING AN INSTANCE TO CLOUD 
NOTED; red= one line of command
Yellow = tools installation do not copy/not a command

Orange= two lines of command, copy the two lines together 
Blue= what to edit within command


Java installation 
sudo apt-get update
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer –y
java –version
installation Jenkins 
wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add –
echo deb http://pkg.jenkins.io/debian-stable binary/ | sudo tee /etc/apt/sources.list.d/jenkins.list
sudo apt-get update
sudo apt-get install jenkins –y
sudo cat /var/lib/jenkins/secrets/initialAdminPassword
Maven installation
sudo apt install maven –y
Tomcat Installation
sudo apt-get update
sudo apt-get install tomcat8 –y
sudo apt-get install tomcat8-docs tomcat8-examples tomcat8-admin –y
sudo cp -r /usr/share/tomcat8-admin/* /var/lib/tomcat8/webapps/ -v
sudo vi /var/lib/tomcat8/conf/server.xml (edit port number)
sudo vi /var/lib/tomcat8/conf/tomcat-users.xml
<role rolename="manager-gui"/>
<role rolename="manager-script"/>
<user username="tomcat" password="password" roles="manager-gui,manager-script"/>
sudo vi /etc/default/tomcat8
comment below command
JAVA_OPTS="-Djava.security.egd=file:/dev/./urandom -Djava.awt.headless=true -Xmx512m -XX:MaxPermSize=512m -XX:+UseConcMarkSweepGC"
sudo systemctl restart tomcat8
sudo systemctl status tomcat8
Creating java web project using maven & setup in bitbucket repo 
Create a repo, add ssh key
ssh-keygen
sudo cat ~/.ssh/id_rsa.pub
copy the generated key into repo and clone the ssh url from your repo
ls –al
cd reponame
mvn archetype:generate -DgroupId=com.mkyong -DartifactId=MyWebApp -DarchetypeArtifactId=maven-archetype-webapp -DinteractiveMode=false
ls -al
git status
git add  *
git commit -m "my first project check-in to bitbucket"
git push
Automate build and deployment of java project using Jenkins
1.	Make sure you configure maven installation under Jenkins-->manage Jenkins-> Global Tool Configuration. under maven installation. enter Maven3 as name, enter path of maven installation --> /usr/share/maven and uncheck install automatically option
2.	install SonarQube scanner,  deploy to container,  Jacoco plugins under Jenkins --> Manage Jenkins --> Manage plug-ins. Click on Available, type Sonarqube, select SonarQube scanner. Click on Install without restart.
3.	Create automate job
4.	under source code mgmt, click git. enter bit bucket URL. go to Bitbucket, select repo you created, clone HTTPS Url. remove git clone from the URL.and add bitbucket credentials
5.	under build trigger click on poll scm, enter this value to check for every 2 mins --> H/02 * * * *
6.	Build --> Add build step --> invoke top level maven targets.select Maven3 from drop down
enter goals as --> clean install 
7.	Click on advanced, enter the path of POM file as --> MyWebApp/pom.xml
8.	add Post build action - select archive the artifacts
9.	enter the below value:(ignore if you get any error in red color) **/*.war
10.	click on Add post build action, select deploy war/ear to container and add **/*.war
in WAR/EAR files. click on Add container , select Tomcat 8.x, click on add credentials, enter tomcat as user name and password as password. select it from drop down. tomcat url should be http://localhost:8090
Sonarqube installation on Ubuntu 
First install Postgres;
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
sudo wget -q https://www.postgresql.org/media/keys/ACCC4CF8.asc -O - | sudo apt-key add -
sudo apt-get -y install postgresql postgresql-contrib
sudo systemctl start postgresql
sudo systemctl enable postgresql
sudo passwd postgres
Enter password as admin
su – postgres
createuser sonar
psql
ALTER USER sonar WITH ENCRYPTED password 'password';
CREATE DATABASE sonar OWNER sonar;
\q 
Type exit
Install sonarqube Webapp
sudo wget https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-6.4.zip
sudo apt-get -y install unzip
sudo unzip sonarqube-6.4.zip -d /opt
sudo mv /opt/sonarqube-6.4 /opt/sonarqube –v
sudo vi /opt/sonarqube/conf/sonar.properties
uncomment the below lines by removing # and add values highlighted yellow
sonar.jdbc.username=sonar
sonar.jdbc.password=password
sonar.jdbc.url=jdbc:postgresql://localhost/sonar
sudo vi /etc/systemd/system/sonar.service
add the below code: 
[Unit]
Description=SonarQube service
After=syslog.target network.target

[Service]
Type=forking

ExecStart=/opt/sonarqube/bin/linux-x86-64/sonar.sh start
ExecStop=/opt/sonarqube/bin/linux-x86-64/sonar.sh stop

User=root
Group=root
Restart=always

[Install]
WantedBy=multi-user.target

sudo systemctl enable sonar
sudo systemctl start sonar
sudo systemctl status sonar
cat /opt/sonarqube/logs/sonar.log
integrating SonarQube with Jenkins
1.	You need to login to SonarQube using admin/admin and click on administration, security, users, click on Tokens, under generate token.Give some value for token name and click on generate. Copy the token.
2.	After installing SonarQube successfully, login to Jenkins. Manage Jenkins --> Configure System --> SonarQube installation
3.	Enter name, URL as http://localhost:9000, paste the token 
4.	Click on Enable injection of Sonarqube server configuration
5.	Click on your existing free style job, click on configure. click on prepare Sonarqube scanner  environment
6.	enter maven goal as clean install sonar:sonar
7.	click on save and build the job
Enabling scanning for PL/SQL files in SonarQube. Code quality check for SQL files using SonarQube
cd /opt/sonarqube/extensions/plugins
sudo wget https://github.com/felipebz/sonar-plsql/releases/download/2.0.0/sonar-plsql-open-plugin-2.0.0.jar
sudo systemctl stop sonar
sudo systemctl start sonar
sudo systemctl status sonar
cat /opt/sonarqube/logs/web.log 
Now login to SonarQube, Navigate to Quality Profiles section, you should see PL/SQL rules added.
Slave nodes helps Jenkins to achieve distributed builds. Ensure Jenkins master is up and running.
Slave node configuration(you need new micro Ubuntu instance for this slave)
sudo apt-get update
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer –y
Install maven
sudo apt-get install maven –y
Create User as Jenkins
sudo useradd -m Jenkins
sudo -u jenkins mkdir /home/jenkins/.ssh
sudo -u jenkins vi /home/jenkins/.ssh/authorized_keys
This will be empty file, now copy the public keys from Jenkins master node into above file by executing below command
sudo cat ~/.ssh/id_rsa.pub
Now go into master node 
ssh jenkins@slave_node_ip
this is to make sure master is able to connect slave node. once you are successfully logged into slave, type exit to come out of slave. Now copy the SSH keys into /var/lib/jenkins/.ssh by executing below command:
sudo cp ~/.ssh/known_hosts  /var/lib/jenkins/.ssh
Register slave node in Jenkins:
Now to go Jenkins Master, manage jenkins, manage nodes
Click on new node. give name and check permanent agent.
give name and no of executors as 1. enter /home/jenkins as remote directory.
select launch method as Launch slaves nodes via SSH.
enter Slave node ip address as Host. click on credentials. Enter user name as jenkins. Make jenkins lowercase as it is shown.Kind as SSH username with private key. enter private key of master node directly by executing below command:
sudo cat ~/.ssh/id_rsa
select Host key verification strategy as "manually trusted key verification strategy"
Click Save.
Click on launch agent..make sure it connects to agent node.
installing SonaType Nexus (please create a new Red Hat Enterprise Linux 7.5 (HVM), SSD Volume Type instance) for storing binaries(WARs, EXEs, DLLs).
please create a new Redhat EC2 instance with Micro type. And open port 8081
sudo yum update –y
sudo yum install wget –y
sudo wget --no-check-certificate --no-cookies --header 'Cookie: oraclelicense=accept-securebackup-cookie' 'http://download.oracle.com/otn-pub/java/jdk/8u141-b15/336fa29ff2bb4ef291e347e091f7f4a7/jdk-8u141-linux-x64.rpm'
Install java
sudo rpm -i jdk-8u141-linux-x64.rpm
sudo /usr/sbin/alternatives --install /usr/bin/java java /usr/java/jdk1.8.0_141/bin/java 20000
sudo /usr/sbin/alternatives --config java
Choose option 2 by typing 2 and enter Execute the below commands -  navigate to /opt directory by changing directory:
cd /opt
Download Nexus
sudo wget https://sonatype-download.global.ssl.fastly.net/nexus/3/nexus-3.0.2-02-unix.tar.gz
Extract Nexus
sudo tar -xvf nexus-3.0.2-02-unix.tar.gz
sudo mv nexus-3.0.2-02 nexus
Create a user called Nexus
sudo adduser nexus
sudo chown -R nexus:nexus /opt/nexus
sudo vi /opt/nexus/bin/nexus.rc
change run_as_user="nexus"
sudo vi /opt/nexus/bin/nexus.vmoptions
replace the entire file with below entry:
-Xms256M
-Xmx256M
-XX:+UnlockDiagnosticVMOptions
-XX:+UnsyncloadClass
-Djava.net.preferIPv4Stack=true
-Dkaraf.home=.
-Dkaraf.base=.
-Dkaraf.etc=etc
-Djava.util.logging.config.file=etc/java.util.logging.properties
-Dkaraf.data=/opt/nexus/nexus-data
-Djava.io.tmpdir=data/tmp
-Dkaraf.startLocalConsole=false
 sudo ln -s /opt/nexus/bin/nexus /etc/init.d/nexus
 sudo chkconfig --add nexus
 sudo chkconfig --levels 345 nexus on
 sudo service nexus start
sudo service nexus status
cat /opt/nexus/nexus-data/log/nexus.log
user name/password is admin/admin123
integrate Sonatype Nexus with Jenkins - Upload artifacts from Jenkins to Nexus repository
Install Nexus Artifact Uploader plugin in Jenkins
Then go to existing job and Add build step, choose Nexus artifact uploader.
Enter below info
Nexus version; NEXUS3
Protocol; HTTP
Nexus URL
Nexus credentials
GroupID; myGrpoupID
Version; 1.0 SNAPSHOT
Repo; maven-snapshots
Artifacts ID; MyWebApp
Type; WAR
File; MyWebApp/target/MyWebApp.war
Save and view changes in components of nexus
Create Pipeline in Jenkins for Automating Builds, Deployment and Code quality checks
Pipeline is groovy based script that have set of plug-ins integrated for automating the builds, deployment and test execution.Pipeline defines your entire build process, which typically includes stages for building an application, testing it and then delivering it. 
Pre-requistes
1. Install Slack, SonarQube plug-in(if already installed, you can skip it)
2. Login to ec2 instance where you installed Jenkins, type the below command
sudo visudo
look for root and add the changes highlighted in red at the below line of root
# User privilege specification
root    ALL=(ALL:ALL) ALL
jenkins ALL=(ALL) NOPASSWD: ALL
After making the changes, Ctrl O, press enter to save.
 Ctrl X to exit the file
Now continue with Pipeline configuration in Jenkins.
1. Login to Jenkins 
2. Create a New item
3. Give name as MyfirstPipelineJob and chose pipeline
4. Click ok. Pipeline is created now 
5. Under build triggers, click on poll SCM,schedule as
H/02 * * * *
Go to Pipeline definition section, click on Pipeline syntax link. under sample step drop down, choose checkout general SCM. enter bitbucket Repository URL, and choose the bitbucket user/password from the drop town. scroll down, click on Generate Pipeline script. Copy the code.
Now copy the below code in green highlighted section into Pipeline section in the pipeline.
node {
def mvnHome = tool 'Maven3'
 stage ('Checkout')  {
copy code here which you generated from step #6
}
stage ('Build') {
           sh "${mvnHome}/bin/mvn clean install -f MyWebApp/pom.xml
    }

stage ('Code Quality scan') {
       withSonarQubeEnv('SonarQube') {
       sh "${mvnHome}/bin/mvn -f MyWebApp/pom.xml sonar:sonar"
        }
}
           
stage ('DEV Deploy')
         {
                echo "deploying to DEV tomcat "
               sh 'sudo cp /var/lib/jenkins/workspace/$JOB_NAME/MyWebApp/target/MyWebApp.war /var/lib/tomcat8/webapps'
          }

stage ('DEV Approve')
      {
            echo "Taking approval from DEV"
                
            timeout(time: 7, unit: 'DAYS') {
            input message: 'Do you want to deploy?', submitter: 'admin'
            } 
     }

stage ('Slack notification') {
      
        slackSend(channel:'channel-name', message: "Job is successful, here is the info -  Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
 
    }
}

8. Change maven and SonarQube variables as highlighted above in red as per your settings.
9. Change the pipeline Job name per your name during deploy stage as highlighted in red.
10.Click Apply, Save
11. Now click on Build. It should build and deploy the code in tomcat.
Terraform Installation steps on Ubuntu 16.0.4
Terraform for provisioning EC2 automatically by writing code instead of manually creating
sudo mkdir -p /opt/terraform
cd /opt/terraform
sudo wget https://releases.hashicorp.com/terraform/0.11.10/terraform_0.11.10_linux_amd64.zip
sudo apt-get install unzip –y
sudo unzip terraform_0.11.10_linux_amd64.zip
sudo vi ~/.bashrc
 add below entry in the last line 
   export PATH="/opt/terraform:$PATH
source ~/.bashrc
terraform –version
Provisioning EC2 instance using Terraform files in AWS
cd ~
mkdir project-terraform
cd project-terraform
sudo vi aws.tf
provider "aws" {
  access_key = "xx"
  secret_key = "xx"
  region     = "us-east-2"
}
terraform init
sudo vi create_ec2.tf
resource "aws_instance" "myFirstInstrance" {
  ami           = "ami-916f59f4"

  key_name = "change_to_your_key_name"
  instance_type = "t2.micro"
  security_groups= [ "security_jenkins_port"]
  tags {
    Name = "jenkins_instance"
  }
}

resource "aws_security_group" "security_jenkins_port" {
  name        = "security_jenkins_port"
  description = "security group for jenkins"

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 8090
    to_port     = 8090
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
 
 ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

 # outbound from jenkis server
  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "security_jenkins_port"
  }
}
terraform plan
terraform apply
type yes
How to install Ansible on Ubuntu. 
The best way to install Ansible for Ubuntu is to add the project's PPA (personal package archive) to your system.
sudo apt-add-repository -y ppa:ansible/ansible
sudo apt-get update
Now you can install Ansible
sudo apt-get install -y ansible
sudo apt install python-pip –y
sudo pip install boto
sudo pip install boto3
sudo apt-get install python-boto –y
pip list boto | grep boto
ansible –version
Ansible Playbook for provisioning Jenkins EC2 instance.
sudo vi ~/.boto
[Credentials]
aws_access_key_id = ??
aws_secret_access_key = ??
sudo vi /etc/ansible/hosts
[localhost]
local
cd ~
mkdir playbooks
cd playbooks
sudo vi create_jenkins_ec2.xml
---
 - name:  provisioning EC2 Lab Exercises using Ansible
   hosts: localhost
   connection: local
   gather_facts: False
   tags: provisioning

   vars:
     keypair: MyinfraCodeKey
     instance_type: t2.micro
     image: ami-916f59f4
     wait: yes
     group: webserver
     count: 1
     region: us-east-2
     security_group: jenkins-security-group
   
   tasks:

     - name: Create a security group
       local_action: 
         module: ec2_group
         name: "{{ security_group }}"
         description: Security Group for webserver Servers
         region: "{{ region }}"
         rules:
            - proto: tcp
              from_port: 22
              to_port: 22
              cidr_ip: 0.0.0.0/0
            - proto: tcp
              from_port: 8080
              to_port: 8080
              cidr_ip: 0.0.0.0/0
            - proto: tcp
              from_port: 80
              to_port: 80
              cidr_ip: 0.0.0.0/0
            - proto: tcp
              from_port: 443
              to_port: 443
              cidr_ip: 0.0.0.0/0
         rules_egress:
            - proto: all
              cidr_ip: 0.0.0.0/0
       register: basic_firewall
     
     - name: Launch the new EC2 Instance
       local_action:  ec2 
                      group={{ security_group }} 
                      instance_type={{ instance_type}} 
                      image={{ image }} 
                      wait=true 
                      region={{ region }} 
                      keypair={{ keypair }}
                      count={{count}}
       register: ec2

     - name: Add the newly created EC2 instance(s) to the local host group (located inside the directory)
       local_action: lineinfile
                     dest="/etc/ansible/hosts"
                     regexp={{ item.public_ip }}
                     insertafter="[webserver]" line={{ item.public_ip }}
       with_items: "{{ ec2.instances }}"

sudo ansible-playbook create_jenkins_ec2.xml
you should see the new instance on AWS console

Automating Jenkins setup
Ansible playbook for java installation
Create ssh key from ansible host
ssh-keygen 
sudo cat ~/.ssh/id_rsa.pub
paste key in ansible node by typing shift A then enter 
sudo vi /home/ubuntu/.ssh/authorized_keys
then go back to ansible node and make make sure you are able to ssh from ansible mgmt node after copying the keys above:
ssh private_ip_of_target_node
type exit to come out of target node. Now make changes to the host file by entering target node IP address
sudo vi /etc/ansible/hosts
[Java_Group]   
xx.xx.xx.xx ansible_ssh_user=ubuntu ansible_ssh_private_key_file=~/.ssh/id_rsa  ansible_python_interpreter=/usr/bin/python3
make changes in playbooks
sudo vi installJava.xml
---
- hosts: Java_Group
  tasks:

  - name: Update APT package manager repositories cache
    become: true
    apt:
      update_cache: yes

  - name: Install Python software prereequesits
    become: yes
    apt: name=python-software-properties

  - name: Install add-apt-repostory
    sudo: yes
    apt: name=software-properties-common state=latest

  - name: Add Oracle Java Repository
    become: yes
    apt_repository: repo='ppa:webupd8team/java'

  - name: Accept Java 8 License
    become: yes
    debconf: name='oracle-java8-installer' question='shared/accepted-oracle-license-v1-1' value='true' vtype='select'

  - name: Install Oracle Java 8
    become: yes
    apt: name={{item}} state=latest
    with_items:
      - oracle-java8-installer
      - ca-certificates
      - oracle-java8-set-default
sudo ansible-playbook installJava.xml
java –version
Installation of Jenkins using Ansible playbook
installing Apache software on nodes using Ansible in Ubuntu EC2
sudo vi installJenkins.xml
---
 - hosts: Java_Group
   
   tasks:
    - name: ensure the jenkins apt repository key is installed
      apt_key: url=https://pkg.jenkins.io/debian-stable/jenkins.io.key state=present
      become: yes

    - name: ensure the repository is configured
      apt_repository: repo='deb https://pkg.jenkins.io/debian-stable binary/' state=present
      become: yes

    - name: ensure jenkins is installed
      apt: name=jenkins update_cache=yes
      become: yes

    - name: ensure jenkins is running
      service: name=jenkins state=started
sudo vi /etc/ansible/hosts
[Jenkins_Group]   
xx.xx.xx.xx ansible_ssh_user=ubuntu ansible_ssh_private_key_file=~/.ssh/id_rsa ansible_python_interpreter=/usr/bin/python3
sudo ansible-playbook installJenkins.xml

Playbook for installing Apache using ansible 
Create SSH keys in Ansible host machine
ssh-keygen
sudo cat ~/.ssh/id_rsa.pub
login to target node, execute the below command to open the file
sudo vi /home/ubuntu/.ssh/authorized_keys
type shift A and then enter and paste the key in the above file
go back to ansible mgmt node, make sure you are able to ssh from ansible mgmt node after copying the keys above:
ssh private_ip_of_target_node
then exit Now in ansible mgmt node, now make changes in host file. Make sure you add public IP address
sudo vi /etc/ansible/hosts
[Apache_Group]   
34.216.41.94 ansible_ssh_user=ubuntu ansible_ssh_private_key_file=~/.ssh/id_rsa ansible_python_interpreter=/usr/bin/python3
cd ~playbooks
sudo vi installApache.xml
---
- hosts: Apache_Group
  become: true
  tasks:
    - name: Install apcahe
      apt: name=apache2 state=present update_cache=yes

    - name: ensure apache started
      service: name=apache2 state=started enabled=yes
now execute the playbook
sudo ansible-playbook installApache.xml
Now enter public ip address or public dns name of target server by in thr browser to see home page of Apache
How to provision EC2 instances in AWS using puppet
Puppet is an Infrastructure provisioning tool
Install puppet master on new Ubuntu with medium instance port 8140
Steps for Puppet Master :
curl -O https://apt.puppetlabs.com/puppetlabs-release-pc1-xenial.deb
sudo dpkg -i puppetlabs-release-pc1-xenial.deb
sudo apt-get update
sudo apt-get install puppetserver -y
sudo ufw allow 8140
sudo systemctl enable puppetserver
sudo systemctl start puppetserver 
sudo systemctl status puppetserver
How to provision EC2 instance using Puppet
you need to install the aws-sdk-core and retries gems as root
sudo /opt/puppetlabs/puppet/bin/gem install aws-sdk-core retries
install AWS SDK
sudo /opt/puppetlabs/puppet/bin/gem install aws-sdk retries
install puppet-labs-aws module
sudo /opt/puppetlabs/bin/puppet module install puppetlabs-aws
sudo mkdir ~/.aws
sudo vi ~/.aws/credentials
[default]
aws_access_key_id = ?
aws_secret_access_key = ?
sudo /opt/puppetlabs/bin/puppet resource ec2_instance
create puppet modules to create new EC2 instance
cd /opt/puppetlabs/puppet/modules/
create directory by
sudo mkdir aws-examples
cd aws-examples
sudo vi create-ec2.pp
ec2_instance { 'My EC2':
    ensure              => present,
    region              => 'us-east-2',
    image_id            => 'ami-916f59f4',
    instance_type       => 't2.micro',
    security_groups     => ['security_jenkins_port'],
    subnet              => 'subnet-aff937d5',
    key_name            => 'mykeyName',

  }
sudo /opt/puppetlabs/bin/puppet apply create-ec2.pp
How to configure puppet agent 
sudo nano /etc/hosts
puppet_master_private ip address    puppet
Press Ctrl O for saving and then enter
Press Ctrl X for exit after saving.
Installing Puppet Agent on server node that Puppet master will manage
wget https://apt.puppetlabs.com/puppetlabs-release-pc1-xenial.deb 
sudo dpkg -i puppetlabs-release-pc1-xenial.deb
sudo apt-get update
sudo apt-get install puppet-agent -y
sudo systemctl enable puppet
sudo systemctl start puppet
sudo systemctl status puppet
Signing certificates on Puppet Master
The first time you run the Puppet agent, it generates an SSL certificate and sends a signing request to the Puppet master. After the Puppet master signs the agent's certificate, it will be able to communicate with and control the agent node
List the unsigned certificates on puppet master EC2 instance(save ip in note pad)
sudo /opt/puppetlabs/bin/puppet cert list
sign the Puppet agent IP address
sudo /opt/puppetlabs/bin/puppet cert sign  "Ec2_private_dns_name"
Verifying installation by creating Manifests in Puppet Master
sudo nano /etc/puppetlabs/code/environments/production/manifests/site.pp
    file {'/tmp/puppet_test.txt':                        # resource type file and filename
    ensure  => present,                             # make sure it exists
    mode    => '0644',                              # file permissions
  content => "hello from Puppet master to agent on ${ipaddress_eth0}!\n",  # Print the eth0 IP fact
    }
Press Ctrl O for saving and then enter
Press Ctrl X for exit after saving
Apply Manifests in Puppet Agent
sudo /opt/puppetlabs/bin/puppet agent –test
sudo cat /tmp/puppet_test.txt
lamp stack installation on puppet node
cd /opt/puppetlabs/puppet/modules
sudo mkdir lamp
cd lamp
sudo mkdir manifests
cd manifests
sudo vi init.pp
class lamp {
# execute 'apt-get update'
exec { 'apt-update':                    # exec resource named 'apt-update'
  command => '/usr/bin/apt-get update'  # command this resource will run
}

# install apache2 package
package { 'apache2':
  require => Exec['apt-update'],        # require 'apt-update' before installing
  ensure => installed,
}

# ensure apache2 service is running
service { 'apache2':
  ensure => running,
}

# install mysql-server package
package { 'mysql-server':
  require => Exec['apt-update'],        # require 'apt-update' before installing
  ensure => installed,
}

# ensure mysql service is running
service { 'mysql':
  ensure => running,
}

# install php7 package
package { 'php7.0-cli':
  require => Exec['apt-update'],        # require 'apt-update' before installing
  ensure => installed,
}

# ensure info.php file exists
file { '/var/www/html/info.php':
  ensure => file,
  content => '<?php  phpinfo(); ?>',    # phpinfo code
  require => Package['apache2'],        # require 'apache2' package before creating
}
}
sudo vi /etc/puppetlabs/code/environments/production/manifests/site.pp
node 'ip-172-31-22-128.us-east-2.compute.amazonaws.com' {
 include lamp
 }
login to puppet agent node
sudo /opt/puppetlabs/bin/puppet agent –test
How to change default interval in puppet agent to ensure it does pull from master after configuration 
Remove apache from puppet agent node
sudo apt-get remove apache2
sudo vi /etc/puppetlabs/puppet/puppet.conf
runinterval = 120
sudo systemctl stop puppet
sudo systemctl start puppet
 Docker Installation on Ubuntu
Launch a new instance, instance shld be small.
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release-cs) stable"
sudo apt-get update
sudo apt-cache policy docker-ce
sudo apt-get install -y docker-ce
sudo systemctl status docker
ps -ef | grep docker
docker –version
sudo docker run hello-world
then create an account on docker or use existing one


